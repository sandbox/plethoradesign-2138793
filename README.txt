Project home page: https://drupal.org/sandbox/plethoradesign/2138793
Release 7.x-1.x-dev

The problem: the Redirect module can result in the occasional "Oops, looks like this request tried to create an infinite loop. We do not allow such things here. We are a professional website!".
This module removes redirects that cause infinite loops; this only applies to redirects created by the Redirect module, and as such this module has just one dependency: 
the Redirect module (https://drupal.org/project/redirect). 

The module hooks intro cron. When cron runs, this module checks for any aliases that clash with existing redirects, and then removes them. It logs these actions to the watchdog table.

Features
- Runs when cron runs
- Logs each redirect deletion to watchdog

To do:
- Drush hook
- Invoke from admin screen for manual run.
- Make cron run optional

Installation / Usage
- make sure you have the Redirect module installed; it is a required dependency.
- install and enable module either via /admin/modules/install or (better yet) with Drush
- make sure you have a properly configured cron job.
- Cron should now remove any infinite loops whenever it runs.
